const express = require('express');
const router = express.Router();
const Task = require('../models/task');

//todas las tareas
router.get('/', async (req, res) =>{
   const tasks = await Task.find();
   console.log(tasks);
   res.json(tasks);
});

//unica tarea
router.get('/:id', async (req, res) =>{
    const tasks = await Task.findById(req.params.id);
    res.json(tasks);
 });

router.post('/', async (req, res) =>{
    // console.log(req.body);
    const {title, description} = req.body;
    const task = new Task({title, description});
    await task.save();
    // console.log(task);
    res.json({status: 'Task Saved'});
});

router.put('/:id', async (req, res) =>{
    const {title, description} = req.body;
    const newTask = {title, description};
    await Task.findByIdAndUpdate(req.params.id, newTask);
    // console.log(req.params.id);
    res.json({status: 'Task Update'});
});

router.delete('/:id', async (req, res) =>{
    await Task.findByIdAndRemove(req.params.id);
    res.json({status: 'Task Deleted'});
})

module.exports = router;