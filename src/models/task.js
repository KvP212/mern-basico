const mongoose = require('mongoose');
const { Schema } = mongoose; // propiedades de la tarea, definido dentro de un schema

const taskSchema = new Schema({
    title: 
        { 
            type: String, 
            required: true
        },

    description:
        {
            type: String,
            required: true
        }
});

module.exports = mongoose.model('Task', taskSchema);