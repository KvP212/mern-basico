import React, { Component } from "react";
import Graphic from "./components/Graphic";
import Header from "./shared/components/layout/Header";
import Content from "./shared/components/layout/Content";

class App extends Component {
    constructor(){
        super();
        this.state = {
            title:"",
            description:"",
            tasks: [],
            _id: ""
        };
        this.addTask = this.addTask.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount(){
        this.fetchTasks();
    }

    fetchTasks(){
        fetch('/api/tasks')
            .then(res => res.json())
            .then(data => {
                console.log(data);
                this.setState({tasks: data});
            })
            .catch(err => console.log(err));
    }

    addTask(e){
        // console.log("Adding task");
        console.log(this.state);
        if(this.state._id){
            fetch(`/api/tasks/${this.state._id}`, {
                method: "PUT",
                body: JSON.stringify(this.state),
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json"
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                
                M.toast({html: data.status});
                this.setState({title:"", description:""});
                this.fetchTasks();
            })
            .catch(err => console.log(err));

        } else {
            fetch('/api/tasks', {
                method: "POST",
                body: JSON.stringify(this.state),
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json"
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                
                M.toast({html: data.status});
                this.setState({title:"", description:""});
                this.fetchTasks();
            })
            .catch(err => console.log(err));
        }

        e.preventDefault();
    }

    deleteTask(id){
        if(confirm("Are you sure you want to delete it?")){
            fetch(`/api/tasks/${id}`, {
                method: "DELETE",
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json"
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                
                M.toast({html: data.status});
                this.fetchTasks();
            })
            .catch(err => console.log(err));
        }
    }

    editTask(id){
        fetch(`/api/tasks/${id}`)
        .then(res => res.json())
        .then(data => {
            console.log(data);
            this.setState({
                title: data.title,
                description: data.description,
                _id: data._id
            });
        })
        .catch(err => console.log(err));
    }

    handleChange(e){
        // console.log(e.target);
        const { name, value } = e.target;
        this.setState({
            [name]: value
        });
    }

    render(){
        return (
            <div>
                {/* Navegacion */}
                <Header/>

                <div className="container">
                    <div className="row">
                        <div className="col s12 m5">
                            <div className="card">
                                <div className="card-content">
                                    <form onSubmit={this.addTask}>
                                        
                                        <div className="row">
                                            <div className="input-field col s12">
                                                <input 
                                                    name="title" 
                                                    onChange={this.handleChange} 
                                                    type="text" 
                                                    placeholder="Task Title"
                                                    value={this.state.title}/>
                                            </div>
                                        </div>

                                        <div className="row">
                                            <div className="input-field col s12">
                                                <textarea 
                                                name="description" 
                                                onChange={this.handleChange} 
                                                placeholder = "Task Description" 
                                                className ="materialize-textarea"
                                                value={this.state.description}>

                                                </textarea>
                                            </div>
                                        </div>

                                        <button type="submit" className="btn light-blue darken-4"> Send </button>

                                    </form>
                                </div>
                            </div>
                        </div>
                        <div className="col s12 m7">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Description</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        this.state.tasks.map( task => {
                                            return (
                                                <tr key={task._id}>
                                                    <td>{task.title}</td>
                                                    <td>{task.description}</td>
                                                    <td>
                                                        
                                                        <button 
                                                            className = "btn light-blue darken-4"
                                                            style = {{margin: "4px"}}
                                                            onClick = {() => this.editTask(task._id)}>
                                                                <i className = "material-icons">edit</i>
                                                        </button>
                                                        
                                                        <button 
                                                            className = "btn light-blue darken-4" 
                                                            style = {{margin: "4px"}}
                                                            onClick = {() => this.deleteTask(task._id)}>
                                                                <i className = "material-icons">delete</i>
                                                        </button>

                                                    </td>
                                                </tr>
                                            )
                                        })
                                    }
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <Content>
                    <Graphic/>
                </Content>
            </div>
        )
    }
}

export default App;