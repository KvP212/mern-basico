import React, { Component } from "react";
import { Chart } from "react-google-charts";

const options = {
  width: "100%",
  height: "100%",
  vAxis: {minValue:0, maxValue:1024},
  pointSize: 10,
  legend: { position: 'bottom' },
  crosshair: { trigger: 'selection', color: "red" },
  animation: {
    duration: 1000,
    easing: 'in'
  }
};

var data = [
  
];

class Graphic extends Component{
  constructor(props) {
    super(props);
    this.state = {
      data: []
    };
    this.getData = this.getData.bind(this);
    this.drawChart = this.drawChart.bind(this);
  }

  componentWillMount(){
    console.log('First this called');
  }

  drawChart(){
    this.setState({data});
  }

  getData(){
    var numX = -1;
    var numY = 0;

    setInterval(() => {

      console.log('Our data is fetched');
      numX++;
      numY = Math.floor(Math.random() * 1024);
      if(data.length > 20){
        data.shift();
      }
      data.push([numX , numY]);
      console.log(data);
      this.drawChart();
    }, 2000)
  }

  componentDidMount(){
    this.getData();
  }

  render(){
      return (
          <div className="container">
              <div className="row">
                  <div className="col s12">
                      <div className="card">
                          <div className="card-content">
                              <span className="card-title">Grafica</span>
                              <Chart
                                  chartType="LineChart"
                                  loader={<div>Loading Chart</div>}
                                  columns={[
                                    {
                                      type: "number",
                                      label: "Time"
                                    },
                                    {
                                      type: "number",
                                      label: "Analog"
                                    }
                                  ]}
                                  rows = {this.state.data} 
                                  options = {options}
                                  legendToggle
                              />
                          </div>
                          <div className="card-action">
                              <a href="/">Get Data</a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      )
  }
}

export default Graphic;
