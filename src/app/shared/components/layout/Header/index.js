import React, { Component } from "react";

class Header extends Component {
    render (){
        return(
            <nav className="light-blue darken-4">
                <div className="container">
                    <a className="brand-logo" href="/">MEARN Stack</a>
                </div>
            </nav>
        )
    }
}

export default Header;