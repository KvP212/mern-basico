const mongoose = require('mongoose');

const URI = 'mongodb://localhost/testdb';
mongoose.connect(URI, { useNewUrlParser: true })
    .then(db => console.log('DB is connected'))
    .catch(err => console.log(err));

exports.module = mongoose;