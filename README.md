# Título del Proyecto

_API REST basico, para inicializar proyecto_

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Mira **Deployment** para conocer como desplegar el proyecto.


### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

```
devDependencies en package.json
```

## Ejecutando las pruebas ⚙️

_Ejecutar las pruebas automatizadas para este sistema_

```
npm run wp
npm run dev
localhost:3003
```

## Deployment 📦

```
node src/index.js
```

## Construido con 🛠️

_MERN_

* [mongo DB](https://github.com/KvP212) - Manejador de DB
* [Express](https://github.com/KvP212) - El framework web usado
* [React](https://github.com/KvP212) - El framework web usado
* [Nodejs](https://github.com/KvP212) - El framework web usado

## Autores ✒️

_Menciona a todos aquellos que ayudaron a levantar el proyecto desde sus inicios_

* **Kev Pineda** - *Trabajo Inicial* - [KvP212](https://github.com/KvP212)

También puedes mirar la lista de todos los [contribuyentes](https://github.com/your/project/contributors) quíenes han participado en este proyecto. 